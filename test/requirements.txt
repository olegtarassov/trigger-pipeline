pytest==4.3.0
flake8==3.7.7
requests==2.21.0

bitbucket-pipes-toolkit==1.14.0
